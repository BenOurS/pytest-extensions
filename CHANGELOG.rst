=========
Changelog
=========

Version 0.0.1 (2022-08-15)
==========================

Features
--------

- First release! :party:
- Introducing comparators: objects equal to any instance of a specific set of types
- New comparators on str: startswith / endswith / contains
